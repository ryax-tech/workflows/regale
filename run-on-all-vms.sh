#!/usr/bin/env bash

hosts="20.56.41.141 20.229.121.156 20.229.176.91"

echo "Arguments $*"
for h in $hosts; do
    echo "===> ssh -i ./slurm-master_key.pem azureuser@$h $*"
    ssh -i ./slurm-master_key.pem azureuser@$h $*
done

