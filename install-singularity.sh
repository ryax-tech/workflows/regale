#!/usr/bin/env bash

#hosts="20.56.41.141" #
hosts="20.229.121.156 20.229.176.91"
for h in $hosts; do
    echo $h
done



SCRIPT_FILE=/tmp/install-singularity.sh

GO_VERSION=1.17.2
OS=linux
ARCH=amd64

VERSION=3.10.0

cat <<EOF >$SCRIPT_FILE
#!/bin/bash

set -x

# Ensure repositories are up-to-date
sudo apt-get update
# Install debian packages for dependencies
sudo apt-get install -y \
   build-essential \
   libseccomp-dev \
   pkg-config \
   squashfs-tools \
   cryptsetup \
   libglib2.0-dev

wget https://dl.google.com/go/go$GO_VERSION.$OS-$ARCH.tar.gz

sudo rm -fr /usr/local/go

sudo tar -C /usr/local -xzvf go$GO_VERSION.$OS-$ARCH.tar.gz && \
    rm go$GO_VERSION.$OS-$ARCH.tar.gz

export GOPATH=\${HOME}/go
export PATH=/usr/local/go/bin:\${PATH}:\${GOPATH}/bin

mkdir -p $GOPATH/src/github.com/sylabs
cd $GOPATH/src/github.com/sylabs
wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-ce-${VERSION}.tar.gz
tar zxvf singularity-ce-${VERSION}.tar.gz
cd ./singularity-ce-${VERSION}
./mconfig
make -C builddir
sudo make -C \$PWD/builddir install
EOF




start_nfs(){
    # After restart to mount nfs again
    for h in $hosts; do
        echo "Mounting NFS on node $h"
        ssh -i $PWD/slurm-master_key.pem azureuser@$h sudo mount -t nfs slurmnfs.file.core.windows.net:/slurmnfs/slurmfileshare /users -o vers=4,minorversion=1,sec=sys
    done
}

execute_cmd(){
    for h in $hosts; do
        scp -i $1 $SCRIPT_FILE $2@$h:$SCRIPT_FILE
        ssh -i $1 $2@$h chmod +x $SCRIPT_FILE
        ssh -i $1 $2@$h $SCRIPT_FILE
        echo $h accessible with user : $2, and key: $1
    done
}

set -e

echo "=========================================================="
echo "Install singularity."
echo "=========================================================="
execute_cmd $PWD/slurm-master_key.pem azureuser


