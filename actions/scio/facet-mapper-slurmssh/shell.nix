with import <nixpkgs> {};
mkShell {
 name = "python-devel";
 venvDir = "venv";

  buildInputs = with python39Packages; [
  pkgs.spark3
  numpy
  pandas
  pyspark
  pyarrow
  dbfread
  tifffile
  venvShellHook
 ];

 postShellHook = ''pip install -r dev-requirements.txt''; }
