#imports
import numpy as np
from datar import dplyr,tidyr
from datar.all import f
import time
import os
import pandas as pd
import pyspark.sql.types
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf, struct

import sys;import site;import functools;sys.argv[0] = '/nix/store/d8ylp8lpc9kp4sc2z1q0apxn2b9f5x2q-python3.9-ryax-module-wrapper-python3-filev1-1/bin/wrapper.py';functools.reduce(lambda k, p: site.addsitedir(p, k), ['/nix/store/d8ylp8lpc9kp4sc2z1q0apxn2b9f5x2q-python3.9-ryax-module-wrapper-python3-filev1-1/lib/python3.9/site-packages','/nix/store/d1yjgc62k5jlhl6gls4mhiqj6dm79plx-python3.9-PyYAML-5.4.1.1/lib/python3.9/site-packages','/nix/store/mg13sradc9ih2bsigsf53sqpry4g6lrh-python3.9-pyspark-3.2.0/lib/python3.9/site-packages','/nix/store/ny856bbprb3a40yf02qfcmda8h3061rq-python3.9-py4j-0.10.9.2/lib/python3.9/site-packages','/nix/store/3w86qcf0yyqkhd0s3vsxzh46108rcdj3-python3.9-pyarrow-6.0.1/lib/python3.9/site-packages','/nix/store/xbp1w6kjwdpmg8d5pznd5ipbx9f0fpiy-python3.9-numpy-1.21.2/lib/python3.9/site-packages','/nix/store/bpzx57pcdn1b69zfnjkp3iw7vkv6vdsz-python3.9-six-1.16.0/lib/python3.9/site-packages','/nix/store/zxk0rsysfqpib19mz8h03xm7hwxq75r5-python3.9-dbfread-2.0.7/lib/python3.9/site-packages','/nix/store/jarc7sf7zqva388641i60pj41v9777qy-python3.9-pandas-1.3.5/lib/python3.9/site-packages','/nix/store/7g8rnj6ljqnvjz6rzw2j12s4w5r9ym08-python3.9-python-dateutil-2.8.2/lib/python3.9/site-packages','/nix/store/b47bpv9flv0r30xayddl5x8i03vv4rry-python3.9-pytz-2021.3/lib/python3.9/site-packages','/nix/store/ji67pbzi8ybk19mmrfg0ciy197794vv5-python3.9-datar-0.8.1/lib/python3.9/site-packages','/nix/store/hdk03iijps5x3psbvmzbakmghn98kip9-python3.9-pipda-0.5.9/lib/python3.9/site-packages','/nix/store/v020sk2kqvpvf0wq60nhcay2ddixli62-python3.9-diot-0.1.4/lib/python3.9/site-packages','/nix/store/nf586622qsp62fik0rz23h9mq8lvbf24-python3.9-inflection-0.5.1/lib/python3.9/site-packages','/nix/store/x7aa9izymwgxd5w3yi4p25xp0vgdkxga-python3.9-pure_eval-0.2.1/lib/python3.9/site-packages','/nix/store/zdy6rgp8sbin0x9zxm7z29fx8i79prjq-python3.9-toml-0.10.2/lib/python3.9/site-packages','/nix/store/7j105ssk2krbw17w7w5vxpf2dd8g5l55-python3.9-varname-0.8.1/lib/python3.9/site-packages','/nix/store/rc54a11yap9hq4iph024qk6m9ncyjg5z-python3.9-setuptools-scm-6.3.2/lib/python3.9/site-packages','/nix/store/y084jrafkfl3q1arhiz6zlxgvja1dw6p-python3.9-packaging-20.9/lib/python3.9/site-packages','/nix/store/qkjla73w2nr8vxkqzhivz8fbiz248lp0-python3.9-pyparsing-2.4.7/lib/python3.9/site-packages','/nix/store/sjb1fkp99xgpvkiplqlx8lzck9gp1y5f-python3.9-tomli-1.2.1/lib/python3.9/site-packages','/nix/store/iz65qglq5i0qpzabq6j40s6vayxgk260-python3.9-executing-0.8.2/lib/python3.9/site-packages','/nix/store/2yvr2wc7p07da0ci1hb93jkcpym915zx-python3.9-tifffile-2021.8.30/lib/python3.9/site-packages','/nix/store/0mz04xbic3jmzmzqhvi0bwn9wlwnbcf9-python3.9-imagecodecs-lite-2019.12.3/lib/python3.9/site-packages'], site._init_pathinfo());


def prep_rule(rule, type_):
    if type_=='crule':
        r = rule.groupby(['f_name','zone']).apply(lambda x: x['attrwt']/np.sum(x['attrwt'])).reset_index()
        r = r.sort_values('level_2').reset_index(drop=True)
        rule['relwt'] = r['attrwt']
        rule = rule[['zone', 'f_name', 'fuzattr', 'relwt']]
    return rule


def get_attr(weti, relief):    
    arule_weti = ['prof', 'plan', 'slope_pct', 'aspect', 'qweti', 'qarea', 'lnqarea', 'new_asp']
    arule_relief = ['pctz2st', 'pctz2pit', 'z2pit']
    attr = weti[arule_weti + ['seqno','zone']]
    
    attr = dplyr.left_join(attr, relief[arule_relief+['seqno']],by='seqno')
    return attr

##----------------------------SPARK RELATED CODE START----------------------------
##--------------------------------------------------------------------------------

# NON-spark function for comparing execution times
def arule_models(model, x, b, b_low, b_hi, b1, b2, d):

    # Calculate fuzzy attributes for each cell
    def basic_model(x,b,d):
        return 1/(1+((x-b)/d)**2)

    if model==1:
        fuzz = basic_model(x, b, d)
    elif model==2:
        fuzz = dplyr.case_when(np.logical_and(x>b_low,x<b_hi),1,
                               x<=b_low,basic_model(x, b_low, d),
                               x>=b_hi,basic_model(x, b_hi, d))
        
    elif model==3:
        fuzz = dplyr.case_when(np.logical_and(x>b1,x<b2),1,
                               x<=b1,basic_model(x, b1, d),
                               x>=b2,basic_model(x, b2, d))
    elif model==4:
        fuzz = dplyr.if_else(x>b, 1, basic_model(x, b, d))
    elif model==5:
        fuzz = dplyr.if_else(x<b, 1, basic_model(x, b, d))
        
    return 100*fuzz

# SPARK function used with sparks' udf 
def arule_models_spark(row):
    
    model = row["model"]
    x = row["x"]
    b = row["b"]
    b_low = row["b_low"]
    b_hi = row["b_hi"]
    b1 = row["b1"]
    b2 = row["b2"]
    d = row["d"]

    # Calculate fuzzy attributes for each cell
    def basic_model(x,b,d):
        return 1/(1+((x-b)/d)**2)

    if model==1:
        fuzz = basic_model(x, b, d)
    elif model==2:
        fuzz = dplyr.case_when(np.logical_and(x>b_low,x<b_hi),1,
                               x<=b_low,basic_model(x, b_low, d),
                               x>=b_hi,basic_model(x, b_hi, d))
        
    elif model==3:
        fuzz = dplyr.case_when(np.logical_and(x>b1,x<b2),1,
                               x<=b1,basic_model(x, b1, d),
                               x>=b2,basic_model(x, b2, d))
    elif model==4:
        fuzz = dplyr.if_else(x>b, 1, basic_model(x, b, d))
    elif model==5:
        fuzz = dplyr.if_else(x<b, 1, basic_model(x, b, d))
        
    return float(100*fuzz)


# Function that calls the spark function to be applied on data
def lsm_fuza(attr, arule, procedure,use_spark=True):
    print(f"========> environment at {__name__} <========")
    for envvar in os.environ:
        print(f"{envvar}={os.environ[envvar]}")


    # Create holder data
    fuzzattr = attr[['seqno', 'new_asp']]
    
    ## condition for using spark in order to compare execution times
    if use_spark==True:

        #----------------------SPARK----------------------#
        # start a spark session
        spark = SparkSession.builder.appName("Spark_TEST").getOrCreate()
        spark.sparkContext.setLogLevel("INFO")
        log4jLogger = spark.sparkContext._jvm.org.apache.log4j
        LOGGER = log4jLogger.LogManager.getLogger(__name__)
        LOGGER.info("pyspark script logger initialized")

        #define as udf the function that must be applied to the data
        #the function is to be applied row-wise and return a float number as result
        arule_models_udf = udf(lambda row: arule_models_spark(row),returnType=pyspark.sql.types.FloatType())
        #start timing the execution
        start_time = time.time()
        ## PARALLEL
        ## this for-loop is necessary for the function must be applied 17 times on the data with different inputs each time
        ## at a later stage this for-loop can be absorbed in a spark workflow but for now the approach is this
        for a in range(len(arule)):
            print(a, arule['class_out'].iloc[a])
            #prepare the dataframe that are going to be fed to spark
            attr_zoned = dplyr.filter(attr, attr['zone']==arule['zone'].iloc[a])
            
            attr_zoned_ps = attr_zoned[arule['attr_in'].iloc[a]].to_frame("x")
            attr_zoned_ps["model"] = arule['model'].iloc[a]
            attr_zoned_ps["b"] = arule['b'].iloc[a]
            attr_zoned_ps["b_low"] = arule['b_low'].iloc[a]
            attr_zoned_ps["b_hi"] = arule['b_hi'].iloc[a]
            attr_zoned_ps["b1"] = arule['b1'].iloc[a]
            attr_zoned_ps["b2"] = arule['b2'].iloc[a]
            attr_zoned_ps["d"] = arule['d'].iloc[a]
            
            # create the spark dataframe 
            attr_zoned_ps = spark.createDataFrame(attr_zoned_ps)
            # call spark execution on the spark dataset created earlier
            attr_zoned_ps = attr_zoned_ps.withColumn(arule['class_out'].iloc[a], arule_models_udf(struct([attr_zoned_ps[x] for x in attr_zoned_ps.columns]))).collect()
            # transform the results to be compatible with local pandas dataframe manipulation
            attr_zoned_ps = spark.createDataFrame(attr_zoned_ps).toPandas()
            # extract the result column produced from the execution
            attr_zoned[arule['class_out'].iloc[a]] = attr_zoned_ps[arule['class_out'].iloc[a]]
            # select specific columns from the produced dataframe
            attr_zoned = attr_zoned[['seqno','zone',arule['class_out'].iloc[a]]]
            # save the selected columns to the final dataframe to be used later by the script
            fuzzattr.loc[attr_zoned['seqno']-1, attr_zoned.columns] = attr_zoned
        
        #stop the time            
        end_time = time.time()
        print("Time (seconds) to execute WITH SPARK: ", end_time - start_time) 
        #close the spark session to free resources
        spark.stop()
    else:
        start_time = time.time()
        ## NON-PARALLEL
        for a in range(len(arule)):
            print(a, arule['class_out'].iloc[a])
            attr_zoned = dplyr.filter(attr, attr['zone']==arule['zone'].iloc[a])
            attr_zoned[arule['class_out'].iloc[a]] = attr_zoned[arule['attr_in'].iloc[a]].apply(lambda row: arule_models(arule['model'].iloc[a], row, arule['b'].iloc[a], arule['b_low'].iloc[a], arule['b_hi'].iloc[a], arule['b1'].iloc[a], arule['b2'].iloc[a], arule['d'].iloc[a]).astype('float32'))
            attr_zoned[arule['class_out'].iloc[a]] = attr_zoned[arule['class_out'].iloc[a]].astype('float32')
            attr_zoned = attr_zoned[['seqno','zone',arule['class_out'].iloc[a]]]
            
            # fuzzattr[f$seqno, names(f)] <- f
            fuzzattr.loc[attr_zoned['seqno']-1, attr_zoned.columns] = attr_zoned
        
        end_time = time.time()
        print("Time (seconds) to execute WITHOUT SPARK: ", end_time - start_time) 
    
    if len([i for i in ['planar_d', 'planar_a'] if i in fuzzattr.columns])==2:
        fuzzattr['planar_2x'] = (fuzzattr['planar_d'] + fuzzattr['planar_a'])/2
        
    # For Second option
    if procedure=='bc_pem':
        raise('TODO in facet_01_lsm.R')
        
        
    return fuzzattr


##------------------------------------------------------------------------------
##----------------------------SPARK RELATED CODE END----------------------------


def fuzc_sum(fuzzattr, crule, first_element_not_buffer,use_spark=True):
    
    t = list(set(crule["fuzattr"]))

    #using d instead of f variable due to imports already using f value
    d = fuzzattr[["seqno", "zone"] + t]
    d = dplyr.arrange(d, f.zone, f.seqno)

    print("!checking zone for None values, example doesn't have any so in a real case here MAY be a BUG!")
    d = dplyr.filter(d, ~pd.isna(d["zone"]))

    seqnos = d["seqno"]

    # db_cleared = db.replace(pd.NA,-9999)

    d = tidyr.nest(d, data=~f.zone)
    d = dplyr.left_join(d, crule, by="zone")

    # attr_zoned[arule['class_out'].iloc[a]] = attr_zoned[arule['attr_in'].iloc[a]].apply(lambda row: arule_models(arule['model'].iloc[a], row, arule['b'].iloc[a], arule['b_low'].iloc[a], arule['b_hi'].iloc[a], arule['b1'].iloc[a], arule['b2'].iloc[a], arule['d'].iloc[a]).astype('float32'))
    
    # !!!!!!!TO BE PARALLELIZED!!!!!!!!!
    start_time = time.time()
    d["data"] = d.apply(lambda row: np.asarray(row["data"][row["fuzattr"]] * row["relwt"]),axis=1)#.astype('float32')
    end_time = time.time()
    print(end_time - start_time)

    r = d.groupby(["zone","f_name"]).apply(lambda x: np.sum(np.array(x["data"].tolist()),axis=0)).reset_index()
    r = r.rename(columns={0:"data"})
    r = pd.pivot_table(r,values="data",index="zone",columns="f_name").reset_index()

    k = tidyr.unnest(r, [x for x in r.columns if x!="zone"])
    
    k.index = np.arange(first_element_not_buffer, first_element_not_buffer + len(k))    
    d = pd.DataFrame(0, index=np.arange(len(fuzzattr)), columns=k.columns)

    d.iloc[k.index] = k
    d["seqno"] = seqnos

    return d


def fuzc_max(fuzc):
    #using D instead of f beracuse f is taken during imports
    d = fuzc[[x for x in fuzc.columns if x not in ["zone", "seqno"]]]
    n = d.columns.tolist()
    
    ## Slower approach
    # start_time = time.time()
    # #assign NA to all values per row except the 2 largest
    # d = d.apply(lambda row: row.nlargest(2),axis=1)
    # #get only the 2 max values and add as column the class from the index (next command will
    # # reverse the index but this is necessery for the command to be executed correctly)
    # d = d.apply(lambda row: row.loc[~np.isnan(row)].reset_index(), axis=1)
    # # assign the column of the classes as index again and transpose the dataframe
    # d = d.apply(lambda row: (row.set_index("index")).transpose())
    # #order the columns' position based on the value of the two max in order to formalize their position ("max_2nd_value", "max_value")
    # d = d.apply(lambda row: row[row.columns[row.loc[row.last_valid_index()].argsort()]])
    # # create the list combining the ordered max values and the respective columns
    # d = d.apply(lambda row: list(row.values[0]) + list(row.columns))
    # # create the final dataframe 
    # d = pd.DataFrame.from_dict(dict(zip(d.index, d.values)),orient="index",columns=["max_2nd_value", "max_value", "max_2nd_facet", "max_facet" ])
    # # end_time = time.time()
    # # print("1st approach total time: ", end_time-start_time)

    def max_2_classes_extractor(row):
        
        names = list(row.index[row.notnull()])
        values = list(row.dropna(axis=0, how='all'))
        
        zipped_lists = zip(values, names)
        sorted_pairs = sorted(zipped_lists)
    
        tuples = zip(*sorted_pairs)
        list1, list2 = [ list(tuple) for tuple in  tuples]
        
        
        return list1 + list2
    
    
    # start_time = time.time()
    d = d.apply(lambda row: max_2_classes_extractor(row.nlargest(2)),axis=1)
    d = pd.DataFrame.from_dict(dict(zip(d.index, d.values)),orient="index",columns=["max_2nd_value", "max_value", "max_2nd_facet", "max_facet" ])
    # end_time = time.time()

    # print("2nd approach total time: ", end_time-start_time)

    return dplyr.bind_cols(fuzc, d)

def lsm_fuzc(fuzzattr, crule,first_element_not_buffer,use_spark=True):
    print("YES")
    k = fuzc_sum(fuzzattr, crule,first_element_not_buffer,use_spark=True)
    # return k
    return fuzc_max(k)
