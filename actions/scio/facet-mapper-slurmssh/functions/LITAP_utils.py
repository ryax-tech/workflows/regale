#imports

# LITAP_utils.R functions
def na_omit(x):
    return x.dropna()

    
def max_na(x):
    
    if sum(np.isnan(x))==len(x):
        y = np.nan
    else:
        y = np.nanmax(x)    
    
    return y