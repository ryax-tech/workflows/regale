#!/usr/bin/env bash

rm -fr green_light*
rm -fr outputs.json
rm -fr outputs/*
rm -fr json_inputs
rm -fr python_outputs
