# regale-facet-mapper-version-2

Running locally.

## ryax-action-wrapper

```sh
cd ryax-action-wrapper
```

## Run locally

* Run manager

```sh
docker run --gpus all --rm -ti --name facet-manager -v tmp:/tmp -v ryax:/home/ryax -v ~/projects/regale/actions/scio/facet-mapper-slurmssh/iodir:/iodir -v ~/projects/regale/actions/scio/facet-mapper-slurmssh/ryax_handler.py:/data/ryax_handler.py -v ~/projects/regale/actions/scio/facet-mapper-slurmssh/env/master.env:/data/default.env facet-mapper-python3-filev1:parallel
```

* Run worker

```sh
docker run --gpus all --rm -ti --name facet-worker -v tmp:/tmp -v ryax:/home/ryax -v ~/projects/regale/actions/scio/facet-mapper-slurmssh/iodir:/iodir -v ~/projects/regale/actions/scio/facet-mapper-slurmssh/ryax_handler.py:/data/ryax_handler.py -v ~/projects/regale/actions/scio/facet-mapper-slurmssh/env/worker.env:/data/default.env facet-mapper-python3-filev1:parallel
```

