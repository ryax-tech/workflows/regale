pyspark==3.2.1
numpy==1.22.3
dbfread==2.0.7
pandas==1.3.4
datar==0.8.1
tifffile==2022.5.4
pyarrow==10.0.0
