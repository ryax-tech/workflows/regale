#!/usr/bin/env bash
############################################################
## WARNING
##
## This script need to be copy and pasted in the ryax action
## custom_script parameter. Then every input defined in ryax
## will automatically become an environment variable with
## same name. As well as other parameters such as:
##
## RYAX_INPUTS_FILE file with the inputs
## RYAX_INPUTS_DIR directory where the inputs are located
## RYAX_OUTPUTS_FILE file with the outputs
## RYAX_OUTPUTS_DIR directory where the outputs are located
##
############################################################

echo "RYAX_PARAM_ARULE="$RYAX_PARAM_ARULE
echo "RYAX_PARAM_CRULE="$RYAX_PARAM_CRULE
echo "RYAX_PARAM_FLOW_ZIP="$RYAX_PARAM_FLOW_ZIP
echo "RYAX_PARAM_FORM_ZIP="$RYAX_PARAM_FORM_ZIP
echo "RYAX_PARAM_JSON_ZIP="$RYAX_PARAM_JSON_ZIP

# Create temporary directory to unzip json simulation jsons
export RYAX_INPUT_FILES_DIR=`mktemp -d -p $RYAX_PREFIX`

SAVED_DIR=`pwd`
cp $RYAX_PARAM_JSON_ZIP $RYAX_INPUT_FILES_DIR
cd $RYAX_INPUT_FILES_DIR
echo "Using input files from $RYAX_INPUT_FILES_DIR"
unzip *.zip
rm -f $RYAX_INPUT_FILES_DIR/*.zip
cd $SAVED_DIR

SCRIPT_FILE=`mktemp -p $RYAX_PREFIX --suffix=-facet-script.sh`

cat > $SCRIPT_FILE <<EOF
#!/usr/bin/env bash
# Input files array
json_input_arr=($RYAX_INPUT_FILES_DIR/*)

echo \`hostname\`" ===> ARR LENGTH   : \${#json_input_arr[@]}"
echo \`hostname\`" ===> SLURM_NPROCS : \$SLURM_NPROCS"
echo \`hostname\`" ===> SLURM_PROCID : \$SLURM_PROCID"

let "TASK_SHARE = \${#json_input_arr[@]} / SLURM_NPROCS"
let "TASK_REMINDER = \${#json_input_arr[@]} % SLURM_NPROCS"
let "BEGIN_INDEX[0] = 0"
let "END_INDEX[0] = TASK_SHARE - 1"
let "LAST_PROCID = SLURM_NPROCS - 1"

if ((\$LAST_PROCID >= 1)) ; then
  for i in \`seq 1 \$LAST_PROCID\`; do
      let "BEGIN_INDEX[i] = END_INDEX[i -1] + 1"
      let "END_INDEX[i] = BEGIN_INDEX[i] + TASK_SHARE - 1"
      if ((i <= TASK_REMINDER)) ; then
          let "END_INDEX[i] = END_INDEX[i] + 1"
      fi
  done
fi

PROC_OUTPUT_DIR=$RYAX_OUTPUTS_DIR/python_outputs/process_\$SLURM_PROCID
mkdir -p \$PROC_OUTPUT_DIR

for i in \`seq \${BEGIN_INDEX[\$SLURM_PROCID]} \${END_INDEX[\$SLURM_PROCID]}\`; do
  EXEC_DIR=\$PROC_OUTPUT_DIR/exec_\$i
  mkdir -p \$EXEC_DIR
  FACET_LOG_FILE=\$EXEC_DIR/facet-\`hostname\`.log


  # Call facet with correct parameters
  echo \`hostname\`"===> Running conda facet application with input "\${json_input_arr[\$i]}
  echo \`hostname\`"===> logs on file \$FACET_LOG_FILE"

  echo "Working on file \${json_input_arr[\$i]}" >> \$FACET_LOG_FILE

  conda run -n facet-env python /home/ryax/facet/ryax_handler.py \
    \${json_input_arr[\$i]} \
    \$RYAX_PARAM_ARULE \
    \$RYAX_PARAM_CRULE \
    \$RYAX_PARAM_FLOW_ZIP \
    \$RYAX_PARAM_FORM_ZIP >> \$FACET_LOG_FILE

  OUTPUT_DIR=\$(cat \$FACET_LOG_FILE | grep OUTPUT_DIR | cut -d'=' -f2)
  cp -fr \$OUTPUT_DIR \$EXEC_DIR
done
EOF

# Change mod and wait a bit to avoid file busy error
sleep 2
chmod +x $SCRIPT_FILE
sleep 2

echo "Running script at $SCRIPT_FILE"

# Tested when ntasks and nodes with same values
srun --nodes=$1 $SCRIPT_FILE

# Export the result directory as the output
export RYAX_OUTPUT_DIRECTORY=$RYAX_OUTPUTS_DIR/python_outputs

