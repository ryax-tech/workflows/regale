def handle(ryax_input):
    ## This is a headless action, all code is in the custom_script
    ## custom_script it is a parameters of hpc addon that allows
    ## to run commands natively supported on the cluster.
    print(f"Headless actions not to be called {ryax_input}")
