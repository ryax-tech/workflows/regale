#!/usr/bin/env bash
#SBATCH --nodes=4

for i in `seq 1 1` ; do
  # Copy unique working directory
  cp -fr $HOME/iodir $HOME/iodir_exp_$i
  export RYAX_WORKING_DIR=$HOME/iodir_exp_$i

  # Simulate RYAX environment variables
  export RYAX_PARAM_ARULE=$RYAX_WORKING_DIR/inputs/arule.csv
  export RYAX_PARAM_CRULE=$RYAX_WORKING_DIR/inputs/crule.csv
  export RYAX_PARAM_JSON_ZIP=$RYAX_WORKING_DIR/inputs/facet_40oz_input.zip
  export RYAX_PARAM_FLOW_ZIP=$RYAX_WORKING_DIR/inputs/flow.zip
  export RYAX_PARAM_FORM_ZIP=$RYAX_WORKING_DIR/inputs/form.zip
  export RYAX_OUTPUTS_DIR=$RYAX_WORKING_DIR
  export RYAX_RESULT_DIR=$RYAX_OUTPUTS_DIR/python_outputs
  export RYAX_PREFIX=/home/ryax/.facet-cache

  # execute srun with i nodes
  echo "==============================================================>"
  echo "==============================================================>"
  echo "Executing srun with $i nodes"
  echo "==============================================================>"
  echo "==============================================================>"
  time $HOME/custom_script.sh $i
done


