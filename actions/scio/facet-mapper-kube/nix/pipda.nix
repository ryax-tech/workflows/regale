{ lib, pkgs, pythonPackages, diot, pure-eval, varname, executing }:

pythonPackages.buildPythonPackage rec {
    pname = "pipda";
    version = "0.5.9";
    name = "${pname}-${version}";

    src = pythonPackages.fetchPypi {
      inherit pname version;
      sha256 = "0127bd2351bb2b565259106b0bbb90d32f64f9703c8356873c9c946531661572";
    };

    propagatedBuildInputs = with pythonPackages; [
      diot
      pure-eval
      varname
      executing
    ];

    doCheck = false;

    meta = with lib; {
      homepage = "";
      description = "A framework for data piping in python";
    };
}
