#flow_mapper.py
import sys
sys.path.insert(1, './functions/')

from zipfile import ZipFile
import os
from pathlib import Path

#imports
from dbfread import DBF
import pandas as pd
import numpy as np
import copy
import string
import datar
from datar import dplyr,tidyr
from datar.all import f
from itertools import compress
import math
import json

#import from other files
from LITAP_functions import *
from LITAP_utils import *
from LITAP_load import *
from LITAP_read_write import *


from flow_calc_ddir import *
from flow_calc_shed import *
from flow_pit_stat_fast import *
from flow_remove_pit_fast import *
from flow_slope_gc import *
import tempfile
import warnings
warnings.filterwarnings("ignore")


#%%
def handle(module_input):
    # input_json = "/home/christos/Desktop/SCiO_Projects/REGALE/regale/code/flow_epirus_3_input_json.json"

    TMP_DIR = "/tmp"

    output_backup_folder = TMP_DIR + "/python_outputs/backup/"
    output_stats_folder = TMP_DIR + "/python_outputs/flow/"

    out_directory = TMP_DIR + "/python_outputs"

    elevation_file = module_input["elevation_file"]     #"../../landmapr/LITAP/inst/extdata/testELEV.dbf"
    nrow = module_input["nrow"]
    ncol = module_input["ncol"]
    missing_value = module_input["missing_value"]
    clim = module_input["clim"]
    rlim = module_input["rlim"]
    verbose = module_input["verbose"]
    resume = None
    max_area = module_input["max_area"]
    max_depth = module_input["max_depth"]

    file = module_input["elevation_file"]

    out_directory = TMP_DIR + "/python_epirus_3_example_outputs/"

    output_flow_folder = out_directory + "flow/"
    output_backup_folder = out_directory + "backup/"

    os.makedirs(output_flow_folder,exist_ok=True)
    os.makedirs(output_backup_folder,exist_ok=True)
    os.makedirs(out_directory + "form/",exist_ok = True)
    os.makedirs(out_directory + "facet/",exist_ok = True)

    if (resume==None): resume=""

    # in order to be compatible with ryax platform, inputs with value -1 are translated to None value
    if clim==-1:
        clim = None
    if rlim==-1:
        rlim = None

    #--------------------------------------------------------------------------------------------        
    #%% ### Loading File and PreProcessing it

    #Load File
    db_start = load_file(file,nrow=nrow,ncol=ncol,missing_value=missing_value,clim=clim,rlim=rlim,verbose=verbose)
    ncol_orig = ncol
    nrow_orig = nrow
    ncol = max(db_start["col"]) - 2
    nrow = max(db_start["row"]) - 2

    #--------------------------------------------------------------------------------------------        
    #%% Calculate Directions
    print("Calculate Directions")
    if (resume=="" or resume=="directions"):
        db_dir = calc_ddir2(copy.deepcopy(db_start),verbose=verbose)
        # db_flats, pit_centres = calc_ddir2(copy.deepcopy(db_start),verbose=verbose)
        
        db_dir.to_csv(output_backup_folder+"dir.csv",index=False)

    #--------------------------------------------------------------------------------------------        
    #%% Calculate watersheds
    print("Calculate watersheds")
    if(resume == "" or resume == "watersheds"):
        db_initial = dict()
        
        db_initial["db"] = calc_shed4(copy.deepcopy(db_dir))

        pit_stats = pit_stat1(copy.deepcopy(db_initial["db"]))
        
        db_initial["stats"] = out_stat(copy.deepcopy(pit_stats))
        # Calc stats for first vol2fl
        db_initial["db"] = calc_vol2fl(copy.deepcopy(db_initial["db"]), i_stats = db_initial["stats"], verbose = verbose)
        
        db_initial["db"].to_csv(output_backup_folder+"initial.csv",index=False)
        db_initial["stats"].to_csv(output_flow_folder+"stats_initial.csv",index=False)

    #--------------------------------------------------------------------------------------------        
    #%% Remove initial pits
    print("Remove initial pits")
    if(resume == "" or resume == "local"):
    #     print(db_initial["db"].iloc[4400:4404])
        db_local = first_pitr1(copy.deepcopy(db_initial["db"]), max_area=max_area,max_depth=max_depth,verbose=verbose)
        stats_local = pit_stat1(copy.deepcopy(db_local))
        stats_local = out_stat(copy.deepcopy(stats_local))
        
        db_local = {
            "db" : db_local,
            "stats" : stats_local
        }
        
        db_local["db"].to_csv(output_backup_folder+"local.csv",index=False)
        # db_local["stats"].to_csv(output_flow_folder+"stats_local.csv",index=False)
        db_local["stats"].to_csv(output_backup_folder+"stats_local.csv",index=False)
        
    #--------------------------------------------------------------------------------------------        
    #%% Calc pond Sheds
    print("Calc pond Sheds")
    if(resume == "" or resume == "pond"):
        
        if (len(np.unique(db_local["db"]["shedno"][~pd.isna(db_local["db"]["shedno"])])) >1):
            db_pond = second_pitr1(copy.deepcopy(db_local["db"]),verbose=verbose)
        else:
            db_pond = dict()
            db_pond["db"] = db_local["db"] >> dplyr.mutate(pond_shed = f.local_shed)
            db_pond["stats"] = None
            raise("TODO: check if this is correct")
            
            
        db_pond["db"].to_csv(output_backup_folder+"pond.csv",index=False)
        # db_pond["stats"].to_csv(output_flow_folder+"stats_pond.csv",index=False)
        db_pond["stats"].to_csv(output_backup_folder+"stats_pond.csv",index=False)
        
    #--------------------------------------------------------------------------------------------        
    #%% Calc fill Sheds
    print("Calc fill Sheds")
    if(resume == "" or resume == "fill"):
        
        if (len(np.unique(db_local["db"]["shedno"][~pd.isna(db_local["db"]["shedno"])])) >1):
            # Add pond sheds details to local sheds
            db_local["db"][["vol2fl", "mm2fl", "parea"]] = db_pond["db"][["vol2fl", "mm2fl", "parea"]]
            db_local["db"]["pond_shed"] = db_pond["db"]["pond_shed"]
            
            db_fill = third_pitr1(copy.deepcopy(db_local["db"]),verbose=verbose)
        else:
            print("  Only a single watershed: No fill outputs")
            db_fill = dict()
            db_fill["db"] = db_pond["db"] >> dplyr.mutate(fill_shed = f.local_shed, vol2fl = 0, mm2fl = 0, parea = 0)
            db_fill["stats"] = pd.DataFrame()
         
            
        # Calculate slope gradients and curvatures
        db_fill["db"] = slope_gc(copy.deepcopy(db_fill["db"]), grid=1)
        
        db_fill["db"].to_csv(output_backup_folder+"fill.csv",index=False)
        # db_fill["stats"].to_csv(output_flow_folder+"stats_fill.csv",index=False)
        
        db_fill["stats"].to_csv(output_backup_folder+"stats_fill.csv",index=False)

        #saving pit file TODO the savings and readings at some point
        if len(db_fill["stats"])>0:
            # Create PIT file
            pit = db_fill["stats"]
            pit = datar.all.filter(pit,f.final==True)
            pit["edge_pit"] = False
            pit = datar.all.arrange(pit,f.shedno)
              
            db_fill["db"].to_csv(output_backup_folder+"pit.csv",index=False)
            # pit.to_csv(output_flow_folder+"stats_pit.csv",index=False)
            pit.to_csv(output_backup_folder+"stats_pit.csv",index=False)

    #--------------------------------------------------------------------------------------------        
    #%% ### Inverted DEM
    print("Inverted DEM")
    def invert(db):
        max_elev = np.amax(db["elev"])
        db["elev"] = max_elev - db["elev"]
        return db

    if(resume == "" or resume == "inverted"):
        db_invert = db_local["db"][["elev", "seqno", "row", "col", "missing", "buffer", "elev_orig", "edge_map"]]
        db_invert = invert(copy.deepcopy(db_invert))
        
        # Inverted Directions
        db_idir = calc_ddir2(db_invert, verbose=verbose)

        db_idir.to_csv(output_backup_folder+"idir.csv",index=False)
        
    #--------------------------------------------------------------------------------------------                
    #%% Inverted Watersheds
    print("Inverted Watersheds")
    if(resume == "" or resume == "iwatersheds"):
        db_iinitial = calc_shed4(copy.deepcopy(db_idir))
        db_iinitial.to_csv(output_backup_folder+"iinitial.csv",index=False)

    #--------------------------------------------------------------------------------------------        
    #%% Invert Remove Initial Pits
    print("Invert Remove Initial Pits")
    db_ilocal = first_pitr1(copy.deepcopy(db_iinitial),max_area = max_area, max_depth = max_depth, verbose = verbose)
    if (len(np.unique(db_ilocal["shedno"][~pd.isna(db_ilocal["shedno"])])) >1):
        ipit = pit_stat1(db_ilocal)
        ipit = out_stat(ipit)
        ipit = dplyr.mutate(ipit,edge_pit=False)
    else:
        ipit = pd.DataFrame()

    #%%    
    db_ilocal.to_csv(output_backup_folder+"ilocal.csv",index=False)
    # ipit.to_csv(output_flow_folder+"stats_ilocal.csv",index=False)
    ipit.to_csv(output_backup_folder+"stats_ilocal.csv",index=False)

    #--------------------------------------------------------------------------------------------        
    #%% Save output in the right format
    save_output(output_backup_folder)
    print("FlowMapR finished execution.")
    #%%
    # db_flats["drop"] = db_flats[["centre","patch"]].groupby("patch").transform(lambda x: not any(x))
    # db_flats = db_flats[db_flats["drop"] == False]
    # db_flats = db_flats.drop(columns=['drop'])

    output_filename = "/tmp/flow_mapper_output.zip"
    with ZipFile(output_filename, "a") as zipfile_ref:
        for root, dirs, files in os.walk(out_directory):
            for filename in files:
                print(f"Adding {Path(root) / filename} to {output_filename}")
                zipfile_ref.write(Path(root) / filename, filename)

    return {'python_outputs' : output_filename}

if __name__ == "__main__":
    handle({
        "nrow": 72,
        "ncol": 110,
        "missing_value": -9999,
        "max_area": 10,
        "max_depth": 3,
        "verbose": false,
        "clim": -1,
        "rlim": -1,
        "elevation_file": "/iodir/inputs/epirus_dem_3_cropped.tif",
    })

# Create the equivalent image of ryax action
# ~/ryax/ryax-main/ryax-action-wrappers/ryax-build $PWD scio-flow latest python3 python3-grpcv1
# Run the image using docker
# docker run -ti --rm -p 8081:8081 scio-flow:latest
# Initiate execution using the client
# ./cli.py --server=localhost:8081 $HOME/projects/regale/actions/scio/flow-mapper/ryax_metadata.yaml --nrow=72 --ncol=110 --missing_value=-9999 --max_area=10 --max_depth=3 --verbose=0 --rlim=-1 --clim=-1 --elevation_file=/home/velho/projects/regale/actions/scio/flow-mapper/data/inputs/epirus_dem_3_cropped.tif