{ lib, pkgs, pythonPackages, pipda }:


pythonPackages.buildPythonPackage rec {
    pname = "datar";
    version = "0.5.6";
    name = "${pname}-${version}";

    src = pythonPackages.fetchPypi {
      inherit pname version;
      sha256 = "ff07aa385f1198f63da22d3cad18b690522cd09aeafd86f90e9b856a1a914a16";
    };

    propagatedBuildInputs = with pythonPackages; [
      pipda
      pandas
    ];

    doCheck = false;

    meta = with lib; {
      homepage = https://github.com/pwwang/datar;
      description = "Port of dplyr and other related R packages in python, using pipda.";
    };
}
