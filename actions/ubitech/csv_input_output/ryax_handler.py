import os
from glob import glob
from pathlib import Path
import time
import json
import subprocess
import socket

# Get spark home from the Nix store
os.environ['SPARK_HOME'] = glob("/nix/store/*-spark-*/lib/spark-*")[0]
SPARK_HOME = os.environ['SPARK_HOME']
os.environ["SPARK_LOG_DIR"] = "/tmp/spark-logs"
os.environ["SPARK_WORKER_DIR"] = "/tmp/spark-worker"

print(f"SPARK_HOME is {SPARK_HOME}")


def handle(ryax_input):
    try:
        slurm_rank = int(os.environ["SLURM_PROCID"])
        slurm_size = int(os.environ["SLURM_NPROCS"])
    except KeyError:
        print("Impossible to retrieve SLURM_PROCID and SLURM_NPROCS")
        raise

    if slurm_size <= 1:
        print(f"This application requires at least 2 nodes to rune currently have only {slurm_size}")

    for var in os.environ:
        print(f"Rank {slurm_rank}: {var}={os.environ[var]}")

    csvio_file = Path(ryax_input["csv_input"])
    os.environ["INPUT_PATH"] = csvio_file.stem
    os.environ["JAR_NAME"] = csvio_file.name
    os.environ["SPARK_WORKER_CORES"] = "2"
    os.environ["SPARK_WORKER_MEMORY"] = "2G"
    os.environ["SPARK_DRIVER_MEMORY"] = "2G"
    os.environ["SPARK_EXECUTOR_MEMORY"] = "2G"
    os.environ["SPARK_LOCAL_IP"] = "0.0.0.0"

    barrier_sleep = 1
    barrier_tries = 1000

    print(f"Rank {slurm_rank}: {json.dumps(ryax_input, indent=4)}")
    output_dir = Path("/iodir/result_csv")
    barrier_file = Path(f"/iodir/green_light_{slurm_rank}.txt")
    master_barrier_file = Path(f"/iodir/green_light_0.txt")
    last_worker_barrier_file = Path(f"/iodir/green_light_{slurm_size-1}.txt")
    barrier_file_end = Path("/iodir/red_light.txt")
    # Only rank 0 download models if needed, others will
    if slurm_rank == 0:
        print(f"Rank {slurm_rank}: total process {slurm_rank} of {slurm_size}")
        if not output_dir.is_dir():
            os.mkdir(output_dir)
        hostname = socket.gethostname()
        ip_address = socket.gethostbyname(hostname)
        print(f"Rank {slurm_rank}: {hostname}")
        print(f"Rank {slurm_rank}: IP address is {ip_address}")
        bash_cmd = ["spark-class", "org.apache.spark.deploy.master.Master"]
        process = subprocess.Popen(bash_cmd, stdout=subprocess.PIPE)
        master_addr = f"spark://{ip_address}:7077"
        with open(barrier_file, "w") as gofile:
            gofile.write(master_addr)
        time.sleep(3)

        # Wait for workers to submit the command
        while not last_worker_barrier_file.is_file() and barrier_tries > 0:
            barrier_tries -= 1
            time.sleep(barrier_sleep)
            print(f"Rank {slurm_rank}: Waiting for workers...")

        submit_bash_cmd = [ "spark-submit", "--class",  "Main", "--master", master_addr, "/data/csvio_2.12-0.1.jar", str(csvio_file), "/tmp/spark_output" ]
        print(f"Rank {slurm_rank}: Submitting job {submit_bash_cmd}...")
        submit_process = subprocess.Popen(submit_bash_cmd, stdout=subprocess.PIPE)
        with open("/tmp/spark-output.csv", "w") as s_file:
            for line in submit_process.stdout:
                s_file.write(line.decode("utf-8"))
        print(f"Rank {slurm_rank}: Process submit finished with waiting code {submit_process.returncode}...")
        with open(barrier_file_end, "w") as gofile:
            gofile.write("finishes all process ungracefully")
        time.sleep(4)
        submit_process.kill()
        process.kill()
        return {"report": "/tmp/spark-output.csv"}
    else :
        with open(barrier_file, "w") as gofile:
            gofile.write("I ma a worker")
        while not master_barrier_file.is_file() and barrier_tries > 0:
            barrier_tries -= 1
            time.sleep(barrier_sleep)
        time.sleep(1)
        with open(master_barrier_file, "r") as gofile:
            master_addr = gofile.read()
        print(f"Rank {slurm_rank}: Connecting to master at {master_addr}...")
        bash_cmd = ["spark-class", "org.apache.spark.deploy.worker.Worker", master_addr]
        worker_process = subprocess.Popen(bash_cmd, stdout=subprocess.PIPE)
        while not barrier_file_end.is_file() and barrier_tries > 0:
            barrier_tries -= 1
            time.sleep(barrier_sleep)
            print(f"Rank {slurm_rank}: Waiting for process to finish...")

        worker_process.kill()

    print(f"Rank {slurm_rank}: Finished...")

    # Load model
    return {"report": "/tmp/empty"}


if __name__ == "__main__":
    handle({
        "csvio_input": "example.csv",
    })
