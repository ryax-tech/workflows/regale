
# Test locally

* Build with ryax filev1

* Launch docker for master

```shell
docker run --gpus all --rm -ti --name spark-master -p 0.0.0.0:7077:7077 -p 0.0.0.0:8080:8080 -p 0.0.0.0:4040-4049:4040-4049 -v tmp:/tmp -v ryax:/home/ryax -v ~/projects/regale/actions/ubitech/csv_input_output/iodir:/iodir -v ~/projects/regale/actions/ubitech/csv_input_output/ryax_handler.py:/data/ryax_handler.py -v ~/projects/regale/actions/ubitech/csv_input_output/env/master.env:/data/default.env csv_input-python3-filev1:latest
```

* Launch docker for worker

```shell
docker run --gpus all --rm -ti --name spark-worker -p 0.0.0.0:8081:8080 -p 0.0.0.0:4050-4059:4050-4059 -v tmp:/tmp -v ryax:/home/ryax -v ~/projects/regale/actions/ubitech/csv_input_output/iodir:/iodir -v ~/projects/regale/actions/ubitech/csv_input_output/ryax_handler.py:/data/ryax_handler.py -v ~/projects/regale/actions/ubitech/csv_input_output/env/worker.env:/data/default.env csv_input-python3-filev1:latest
```

