import json


from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils  # Spark streaming Kafka receiver
from kafka import KafkaProducer # Kafka Producer

from termcolor import cprint  # Colors in the console output
"""
    Global variables
"""
kafka_server = "212.101.173.176:29092"
zookeeper = "212.101.173.176:2181"
application_name = 'regale_packetbeat_handler'
input_topic = "regale_packetbeat"

# Set method parameters:
kafka_partitions = 1  # Number of partitions of the input Kafka topic
threshold_ratio = 0.4
maximum_number_bytes = 1000
threshold = 50  # Minimal increase of receive/sent packets ratio
minimal_incoming = 100000  # Minimal count of incoming packets
long_window_length = 30  # Window length for average ratio computation (must be a multiple of microbatch interval)
base_window_length = 5  # Window length for basic computation (must be a multiple of microbatch interval)
output_topic = "regale_attacker_info"


#Receive input from Kafka client
sc = SparkContext(appName='regale_packetbeat_input')  # Application name used as the appName
sc.setLogLevel("ERROR")
n=5
ssc = StreamingContext(sc, n)  # Spark microbatch is n seconds




def send_to_kafka(data, producer, topic):
    """
    Send given data to the specified kafka topic.
    :param data: data to send
    :param producer: producer that sends the data
    :param topic: name of the receiving kafka topic
    """
    producer.send(topic, str(data).encode('utf-8'))



def print_and_send(rdd, producer, topic):
    """
    Transform given computation results into the JSON format and send them to the specified host.
    JSON format:
        {"@type": "detection.ddos", "host" : <sourt_ip> "short_bytes_in" : <s_bytes_int>,
        "short_bytes_out": <short_bytes_out>,"longt_bytes_in" : <l_bytes_int>,  "long_bytes_out" : <l_bytes_out>,  "attackers": [set of attackers]}
    :param rdd: rdd to be parsed and sent
    :param producer: producer that sends the data
    :param topic: name of the receiving kafka topic
    """
    results = ""

    print("=======================")

    rdd_map = rdd.collectAsMap()
    print(rdd_map)
    # generate JSON response for each aggregated rdd
    for key, val in rdd_map.items():
        source = key[0]
        dest = key[1]
        s_bytes_in = val[0][0]
        s_bytes_out = val[0][1]
        l_bytes_in = val[1][0]
        l_bytes_out = val[1][1]

        print(source)
        print(dest)
        print(l_bytes_out)
        print(s_bytes_out)

        new_entry = {"@type": "detection.ddos",
                      "server": dest,
                      "short_duration_bytes_in": s_bytes_in,
                      "short_duration_bytes_out": s_bytes_out,
                      "long_duration_bytes_in": l_bytes_in,
                      "long_duration_bytes_out": l_bytes_out,
                      "attackers": source}

        results += ("%s\n" % json.dumps(new_entry))

        # # Print results to stdout
        cprint(results)

        # # Send results to the specified kafka topic
        send_to_kafka(results, producer, topic)
        print("sent to kafka ")





def receive_data():

    # Initialize input DStream of flows from specified Zookeeper server and Kafka topic
    input_stream = KafkaUtils.createDirectStream(ssc, [input_topic], {"bootstrap.servers": kafka_server})
    return input_stream

def process_data(input_stream):

    #Process inputStream get Json form
    parsed = input_stream.map(lambda v: json.loads(v[1]))


    # Filter and get json  with the values needed
    filtered = parsed.filter(lambda json_rdd: "client" in json_rdd.keys() and
                                                "url" in json_rdd.keys() and
                                                "network" in json_rdd.keys() and
                                                "method"  in json_rdd.keys() and
                                                "source" in json_rdd.keys() and
                                                "destination" in json_rdd.keys())


    """
        Example of stream after map
        (client,dest,protocol,method)
        ('10.0.8.51', '212.101.173.176', 'http', 'GET')
    """

    stractured_data = parsed.map(lambda x: (str(x['client']['ip']),
                                            str(x['url']['domain']),
                                            str(x['network']['protocol']),
                                            str(x['method']),
                                            int(x['source']['bytes']),
                                            int(x['destination']['bytes'])))

    #Print stractured data
    small_window = stractured_data.window(base_window_length, base_window_length)

    """
    x-->(client,dest,protocol,method,source bytes,response bytes)
    protocol and method omitted
    """
    pairs = small_window.map(lambda x: ((x[0],x[1]),(x[4],x[5])))
    """
    x===(client,dest,bytes_in,bytes_out)
    """
    aggregated_small_window = pairs.reduceByKey(lambda x,y: (x[0]+y[0],x[1]+y[1]))

    long_window = aggregated_small_window.window(long_window_length, base_window_length)
    aggregated_long_window = long_window.reduceByKey(lambda x,y: (x[0]+y[0],x[1]+y[1]))
    """
    Data in the form of ( (source,dest)  (s_bystes_in) (sbytes_outs)   (long_bytes_in) (long_bytesout) 
    """

    #Print union
    #aggregated_long_window.pprint()

    windows_union = aggregated_small_window.join(aggregated_long_window)


    #Produce malicius ips
    windows_union.map(lambda x : " source  " +str(x[1][0][0]) + " destination  " + str(x[1][0][1]) + " bytes_in" + str(x[1][1][0]) + " bytes_out "+ str(x[1][1][1])).pprint()
    nonzero_union = windows_union.filter(lambda rdd: rdd[1][0][1] != 0 and rdd[1][1][1] != 0 and rdd[1][0][0]!=0 and rdd[1][1][1]!=0)

    malicious_ips = nonzero_union.filter(lambda  rdd : rdd[1][1][1] * threshold_ratio < rdd[1][0][1]  and rdd[1][1][1] > maximum_number_bytes )

    attackers_info = malicious_ips
    malicious_ips.map(lambda  x: " malicious ips is " +str(x[0][0])).pprint()


    attackers_info.map(lambda  x : "the attackers info is " + str(x)).pprint()

    return attackers_info



def send_data(attackers_info):
    kafka_producer = KafkaProducer(bootstrap_servers=kafka_server,
                                  client_id="spark-producer-" + application_name,
                                  )

    attackers_info.foreachRDD(lambda rdd: print_and_send(rdd, kafka_producer, output_topic))






if __name__ == "__main__":


    """
        Source Part (needs spark libraries)
    """
    input_stream = receive_data()


    """
        Process Part 
    """
    attackers_info = process_data(input_stream)

    """
       Produce Part(needs spark libraries)
    """
    send_data(attackers_info)

    ssc.start()
    ssc.awaitTermination()