# DDoS Attack Window Application

Based on Spark Window functionality, Kafka JSON messages
are consumed and then processed in order to identify if 
a server is under attack. The infrastructure to retrieve data
that allow such processing is based on Elasticsearch and ELK stack in
general. A server(or multiple) running Packetbeat sends messages
via Kafka. Consumers then can retrieve information such as incoming and outgoing
bytes on specific ports. Long-term and short-term windows combined can reveal the ratio of ingress/egress behavior. In that manner, a network spike can be identified, and thus malicious IP being banned.


## How to run
In order to run : 
```
spark-submit  --packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.2.0   --jars spark-streaming-kafka-assembly_2.10-1.6.1.jar   Stream_App.py
```