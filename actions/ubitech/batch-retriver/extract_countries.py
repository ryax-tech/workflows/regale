# coding=utf8
# the above tag defines encoding for this document and is for Python 2.x compatibility

import re

regex = r"\d+\s+(\w+)\s+(\w{2})\s.*"

test_str = ("040 	Austria 	AT 	AUT\n"
	"056 	Belgium 	BE 	BEL\n"
	"100 	Bulgaria 	BG 	BGR\n"
	"191 	Croatia 	HR 	HRV\n"
	"196 	Cyprus 	CY 	CYP\n"
	"203 	Czech Republic 	CZ 	CZE\n"
	"208 	Denmark 	DK 	DNK\n"
	"233 	Estonia 	EE 	EST\n"
	"246 	Finland 	FI 	FIN\n"
	"250 	France 	FR 	FRA\n"
	"276 	Germany 	DE 	DEU\n"
	"300 	Greece 	GR 	GRC\n"
	"348 	Hungary 	HU 	HUN\n"
	"372 	Ireland, Republic of (EIRE) 	IE 	IRL\n"
	"380 	Italy 	IT 	ITA\n"
	"428 	Latvia 	LV 	LVA\n"
	"440 	Lithuania 	LT 	LTU\n"
	"442 	Luxembourg 	LU 	LUX\n"
	"470 	Malta 	MT 	MLT\n"
	"528 	Netherlands 	NL 	NLD\n"
	"616 	Poland 	PL 	POL\n"
	"620 	Portugal 	PT 	PRT\n"
	"642 	Romania 	RO 	ROU\n"
	"703 	Slovakia 	SK 	SVK\n"
	"705 	Slovenia 	SI 	SVN\n"
	"724 	Spain 	ES 	ESP\n"
	"752 	Sweden 	SE 	SWE\n"
	"826 	United Kingdom 	GB 	GBR")

matches = re.finditer(regex, test_str, re.MULTILINE)

for matchNum, match in enumerate(matches, start=1):
    #print ("Match {matchNum} was found at {start}-{end}: {match}".format(matchNum = matchNum, start = match.start(), end = match.end(), match = match.group()))
    print (f'("{match.group(2)}", "{match.group(1)}"),')
    #for groupNum in range(0, len(match.groups())):
        #groupNum = groupNum + 1
        #print ("Group {groupNum} found at {start}-{end}: {group}".format(groupNum = groupNum, start = match.start(groupNum), end = match.end(groupNum), group = match.group(groupNum)))

# Note: for Python 2.7 compatibility, use ur"" to prefix the regex and u"" to prefix the test string and substitution.
