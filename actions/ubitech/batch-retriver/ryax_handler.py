#!/usr/bin/python3

from kafka import KafkaConsumer
import json


#Write output data to file
#TODO:
def handle(input):
    # Set method parameters:
    kafka_server = input["kafka_server"]
    input_topic = input["input_topic"]
    max_messages = input["max_messages"]

    # Define server with port
    bootstrap_servers = [kafka_server]

    # Initialize consumer variable
    print(f"Listening on {kafka_server} topic {input_topic}, until reach {max_messages} messages")
    consumer = KafkaConsumer (input_topic,group_id='group1',bootstrap_servers = bootstrap_servers)
    data_file = "/tmp/datafile.txt"
    with open(data_file,"w+",encoding='utf-8') as f:
        data = []
        for msg in consumer:
            try:
                message_decoded = json.loads(msg.value.decode('utf-8'))
                data.append(message_decoded)
                print(message_decoded)
                if len(data) >= max_messages:
                    print(f"Maximum number of messages {max_messages} reached")
                    break
            except json.decoder.JSONDecodeError as err:
                print("Received a non json message, ignoring...")
        json.dump(data,f,ensure_ascii=False,indent=4)
    consumer.close()

    return { "data_output_file": data_file }

if __name__ == "__main__":
    input_json = {
        "kafka_server": "212.101.173.176:29092",
        "input_topic": "batch_topic",
        "max_messages": 10,
    }
    handle(input_json)

