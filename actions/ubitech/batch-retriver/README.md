Reads batche messages from Kafka server and copy them to a file... cronjob-style.

To enable and start the cronjob: 
```python
  python3 main.py

  crontab -e 
```
The crontab command lists all cronjobs running. 

To display Kafka Queue messages
```bash 
kafkacat -C -b 212.101.173.176:29092 -t example2
```
