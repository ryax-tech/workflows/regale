from glob import glob
import sys
import os

# Get spark home from the Nix store
os.environ['SPARK_HOME'] = glob("/nix/store/*-spark-*/lib/spark-*")[0]
SPARK_HOME = os.environ['SPARK_HOME']
print(f"SPARK_HOME is {SPARK_HOME}")

# Fix Pyspark python version
os.environ['PYSPARK_PYTHON'] = sys.executable
os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable
sys.path.append(SPARK_HOME + "/python")

from pyspark.sql import SparkSession

def handle(input):
    # Set method parameters:
    threshold_ratio = 0.9
    maximum_number_bytes = 65000  # The threshold for the second condition to be determined as an attacker

    path = input['json_input']
    output_path = "/tmp/json.data"

    print("Starting spark...")

    spark = SparkSession.builder \
        .appName("SparkByExamples.com") \
        .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    #Read multiline comment from python3
    df = spark.read.option("multiline","true").json(path)

    #Print Element
    df.select(df['server']).show()

    #The final processing part of the algorithm
    df.rdd.map(lambda x :  "Check if is attacker " + str(x["long_duration_bytes_out"]) + "*" + str(threshold_ratio) + "<" + str(x["short_duration_bytes_out"]) +
       " " + str(x["short_duration_bytes_out"]) + ">" +
       str(maximum_number_bytes)).collect()

    #Get suspicious IPs
    malicious_ip_info = df.rdd.filter(
        lambda rdd: rdd["long_duration_bytes_out"] * threshold_ratio < rdd["short_duration_bytes_out"] and rdd["short_duration_bytes_out"] > maximum_number_bytes)

    print("Saving folder.")

    #Save to folder
    spark.createDataFrame(malicious_ip_info).write.mode("overwrite").format("json").save(output_path)

    return { "json_output" : output_path }
