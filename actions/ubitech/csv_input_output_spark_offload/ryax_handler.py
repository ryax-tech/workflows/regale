import os
import time
from glob import glob
from pathlib import Path
import subprocess


# Get spark home from the Nix store
os.environ['SPARK_HOME'] = glob("/nix/store/*-spark-*/lib/spark-*")[0]
SPARK_HOME = os.environ['SPARK_HOME']
os.environ["SPARK_LOG_DIR"] = "/tmp/spark-logs"
os.environ["SPARK_WORKER_DIR"] = "/tmp/spark-worker"
os.environ["SPARK_SSH_OPTS"] = "-p 2222 -o StrictHostKeyChecking=no"

print(f"SPARK_HOME is {SPARK_HOME}")

with open('/etc/hostname') as f:
    pod_name = f.read()
print(f"Pod HOSTNAME is ========> {pod_name}")
os.environ["SPARK_LOCAL_HOSTNAME"] = pod_name

# Generate keys

def handle(ryax_input):
    csvio_file = Path(ryax_input["csv_input"])

    print(f"Working on file {csvio_file}...")
    os.environ["INPUT_PATH"] = csvio_file.stem
    os.environ["JAR_NAME"] = csvio_file.name
    os.environ["SPARK_WORKER_CORES"] = "2"
    os.environ["SPARK_WORKER_MEMORY"] = "2G"
    os.environ["SPARK_DRIVER_MEMORY"] = "2G"
    os.environ["SPARK_EXECUTOR_MEMORY"] = "2G"
    os.environ["PYTHONHASHSEED"] = "0"

    # Create host keys
    Path("/tmp/.ssh").mkdir(parents=True, exist_ok=True)
    print("Creating RSA host keys...")
    subprocess.Popen([
        'ssh-keygen',
        '-q',
        '-N', '""',
        '-t', 'rsa',
        '-b', '4096',
        '-f', '/tmp/.ssh/ssh_host_rsa_key'
    ], stdout=subprocess.PIPE)
    print("Creating ed25519 host keys...")
    subprocess.Popen([
        'ssh-keygen',
        '-q',
        '-N', '""',
        '-t', 'ed25519',
        '-f', '/tmp/.ssh/ssh_host_rsa_key'
    ], stdout=subprocess.PIPE)

    sshd_bin = glob("/nix/store/*-openssh-*/bin/sshd")[0]
    print("Running sshd...")
    sshd_process = subprocess.Popen([
            sshd_bin,
            "-D",
            "-e",
            "-f",
            "/data/ssh/sshd_config"
        ],
        stdout=subprocess.PIPE
    )
    if sshd_process.stderr is not None:
        for line in sshd_process.stderr:
            print("STDERR==> "+line)
    time.sleep(1)

    Path("/tmp/spark_output").mkdir(parents=True, exist_ok=True)


    submit_bash_cmd = [
        f"{os.environ['SPARK_HOME']}/bin/spark-class",
        "org.apache.spark.deploy.SparkSubmit",
        "--conf",
        "spark.executor.extraJavaOptions=-verbose:class",
        "--conf",
        "spark.kubernetes.namespace=ryaxns-execs",
        "--master",
        "k8s://https://kubernetes.default:443",
        "--deploy-mode",
        "client",
        "--conf",
        f"spark.driver.host=localhost",
        "--conf",
        "spark.driver.port=4041",
        "--conf",
        "spark.driver.bindAddress=0.0.0.0",
        "--class",
        "Main",
        "--conf",
        "spark.kubernetes.container.image=ryaxtech/spark-on-k8s:v0.1.0",
        "--conf",
        "spark.kubernetes.executor.podTemplateFile=file:///data/executor-pod-template.yaml",
        "--conf",
        "spark.executor.instances=8",
        "--conf",
        "spark.kubernetes.authenticate.driver.serviceAccountName=spark",
        "file:///data/csvio_2.12-0.1.jar",
        f"file://{csvio_file}",
        "file:///tmp/spark_output",
    ]

    # Call spark submit write stdout as the output file
    submit_process = subprocess.Popen(submit_bash_cmd, stdout=subprocess.PIPE)
    if submit_process.stderr is not None:
        for line in submit_process.stderr:
            print("STDERR==> "+line)

    with open("/tmp/spark-output.txt", "w") as s_file:
        for line in submit_process.stdout:
            print(line)
            s_file.write(line.decode("utf-8"))

    # Load modelspark
    return {"report": "/tmp/spark-output.txt"}


if __name__ == "__main__":
    handle({
        "csvio_input": "/iodir/example.csv",
    })
