#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import os
import sys
from glob import glob
from random import random
from operator import add
from pathlib import Path
import time

# Get spark home from the Nix store
os.environ['SPARK_HOME'] = glob("/nix/store/*-spark-*/lib/spark-*")[0]
SPARK_HOME = os.environ['SPARK_HOME']
print(f"SPARK_HOME is {SPARK_HOME}")

# Fix Pyspark python version
os.environ['PYSPARK_PYTHON'] = sys.executable
os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable
sys.path.append(SPARK_HOME + "/python")

from pyspark import SparkContext
from pyspark.conf import SparkConf


def f(_):
    x = random() * 2 - 1
    y = random() * 2 - 1
    return 1 if x ** 2 + y ** 2 <= 1 else 0


def handle(inputs):
    """
    Preparing phase
    """
    # discover rank (unique id on the set) and size (number of process on the set)
    slurm_rank = 0
    slurm_size = 1
    # if fails executes with a single processor
    try:
        slurm_rank = int(os.environ["SLURM_PROCID"])
        slurm_size = int(os.environ["SLURM_NPROCS"])
    except KeyError:
        pass
    partitions = int(inputs["partitions"])/slurm_size
    threads = 4
    output_dir = Path("/iodir/partial_result_pi")
    output_file = output_dir / f"computedpi-{slurm_rank}.txt"
    barrier_file = Path("/iodir/green_light.txt")
    if slurm_rank == 0:
        partitions += int(inputs["partitions"]) % slurm_size
        if not output_dir.is_dir():
            os.mkdir(output_dir)
        with open(barrier_file, "w") as gofile:
            gofile.write("this is the way")
    else:
        # make workers wait for the output folder to be ready and created
        barrier_sleep = 1
        barrier_tries = 1000
        while not barrier_file.is_file() and barrier_tries > 0:
            barrier_tries -= 1
            time.sleep(barrier_sleep)

    """
    Execution phase
    """
    multiplier = 1000
    conf = SparkConf()
    sc = SparkContext(master=f"local[{threads}]", appName="PythonPi", conf=conf)
    print(f"[Rank {slurm_rank}] Partitions = {partitions}")
    n = multiplier * partitions
    print(f"[Rank {slurm_rank}] Number of points to generate N = {n}")
    count = sc.parallelize(range(1, n + 1), partitions).map(f).reduce(add)
    print(f"[Rank {slurm_rank}] Total {count} points inside out of {n} generated points")
    with open(output_file, "w") as pi_output:
        pi_output.write(str(count))
    sc.stop()

    """
    Wait for results phase
    """
    # if there are more workers need to wait for others to finish
    if slurm_rank == 0 and slurm_size > 1:
        retries = 10000
        retry_interval = 1
        # wait until the number tagged_images match the full set of input images
        while len(os.listdir(output_dir)) >= slurm_size and retries > 0:
            print(
                f"[Rank {slurm_rank}] Waiting for result files, have {len(os.listdir(output_dir))} out of {slurm_size}")
            retries -= 1
            time.sleep(retry_interval)
            print(f"[Rank {slurm_rank}] Retrying {retries}")
        if len(os.listdir(output_dir)) == slurm_size:
            print(f"[Rank {slurm_rank}] Results {len(os.listdir(output_dir))} out of {slurm_size}")
        else:
            print(f"[Rank {slurm_rank}] ERROR have {len(os.listdir(output_dir))} out of {slurm_size}")

    """
    Geather results phase
    """
    if slurm_rank == 0:
        for result_file in os.listdir(output_dir):
            with open(result_file, "r") as result_count:
                count += int(result_count.read())
        pi = (4.0 * count) / (int(inputs["partitions"]) * multiplier)
        final_result_file = "/tmp/final-result.txt"
        with open(final_result_file, "w") as _:
            _.write(pi)
        print(f"[Rank {slurm_rank}] Final pi is {pi}")
        return {"pi": pi, "sparks_result_file": final_result_file}
    else:
        empty_file = Path("/tmp/empty_file.txt")
        with open(empty_file, 'w') as _:
            pass
        pi = 4.0*count/n
        print(f"[Rank {slurm_rank}] Partial pi is {pi}")
        return {"pi": pi, "sparks_result_file": empty_file}

