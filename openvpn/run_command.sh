#!/usr/bin/env bash
servers="regale-vm1 regale-vm2 regale-vm3 regale-vm4 regale-vm5"
for s in $servers; do
    ssh $s $@
done
