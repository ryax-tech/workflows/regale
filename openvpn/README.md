
# OpenVPN

## How to connect

Make sure you have all 4 files needed to connect en their full path.

* /home/user/openvpn/ca.crt
* /home/user/cslab-openstack-ryax-regale.crt
* /home/user/cslab-openstack-ryax-regale.csr
* /home/user/cslab-openstack-ryax-regale.key


Make sure your config looks like the one below:

```config
client
dev tun
proto udp
remote cloud-gw.cslab.ece.ntua.gr 1194
resolv-retry infinite
nobind
user nobody
group nogroup
persist-key
persist-tun

# edit the following lines with your paths
ca /home/velho/projects/regale/openvpn/ca.crt
cert /home/velho/projects/regale/openvpn/cslab-openstack-ryax-regale.crt
key /home/velho/projects/regale/openvpn/cslab-openstack-ryax-regale.key

ns-cert-type server
comp-lzo

# Set log file verbosity.
verb 3

# Required to accept legacy ciphers like BF-CBC
providers legacy default

# Need to add BF-CBC, only cipher accepted
data-ciphers AES-256-GCM:AES-128-GCM:BF-CBC

# Avoid passing all traffic through the VPN
redirect-gateway
```

Start the vpn by using the command below, root is required because it will create a tun interface.

```sh
sudo openvpn --config cslab-openstack-vpn.ovpn
```

## Nodes

```
  83   │ Host regale-vm1-slurm-master
  84   │   User ubuntu
  85   │   HostName 10.0.1.215
  86   │
  87   │ Host regale-vm2-slurm-node-1
  88   │   User ubuntu
  89   │   HostName 10.0.1.200
  90   │
  91   │ Host regale-vm3-slurm-node-2
  92   │   User ubuntu
  93   │   HostName 10.0.1.209
  94   │
  95   │ Host regale-vm4-k3s-master
  96   │   User ubuntu
  97   │   HostName 10.0.1.217
  98   │
  99   │ Host regale-vm5-k3s-node
 100   │   User ubuntu
 101   │   HostName 10.0.1.211
 102   │
```


Access Ryax server: https://10.0.1.211/#/login

Ubitech
gledakis:dW96deGu3Dq7yF4
iplakas:dW96deGu3Dq7yF5

SCIO
antonis:dW96deGu3Dq7yF6
christos:dW96deGu3Dq7yF7

pedro.velho:dW96deGu3Dq7yF7
