
# Performance Evaluation

## Deploy pod

```shell
kubectl apply -f facet-pod.yaml
kubectl apply -f facet-service.yaml
```

## Setup ryax-cli

Install wrapper.

```sh
git clone wrapper
cd wrapper
python -m venv .venv
source .venv/bin/activate
poetry install
poetry shell

# go to cli directory
cd ./wrappers/python3-grpcv1/ 

# check command is running
./cli.py --help
```

## Dispatch execution

```sh
./cli.py --server=localhost:8081 $FACET_HOME/ryax_metadata.yaml --arule=$FACET_HOME/inputs/arule.csv --crule=$FACET_HOME/inputs/crule.csv --input_json=$HOME/regale/big_input/facet_dem_450_input_json.json --flow_zipped_files=$HOME/regale/big_input/flow.zip --form_zipped_files=$HOME/regale/big_input/form.zip --executor_instances=40 --executor_image=ryaxtech/spark-on-k8s:facet-0.0.1 --executor_pod_template=$FACET_HOME/executor-pod-template.yaml --driver_headless_service=sparkpidriver
```

## Automation

```sh
for nprocs in 150 200 250 300 350 400 ; do 
  ./cli.py --server=localhost:8081 $FACET_HOME/ryax_metadata.yaml \
    --arule=$FACET_HOME/inputs/arule.csv \
    --crule=$FACET_HOME/inputs/crule.csv \
    --input_json=$HOME/regale/big_input/facet_dem_450_input_json.json \
    --flow_zipped_files=$HOME/regale/big_input/flow.zip \
    --form_zipped_files=$HOME/regale/big_input/form.zip \
    --executor_instances=$nprocs \
    --executor_image=ryaxtech/spark-on-k8s:facet-0.0.1 \
    --executor_pod_template=$FACET_HOME/executor-pod-template.yaml \
    --driver_headless_service=sparkpidriver
```

# Results

## regale.ryax.io (Azure nodes)

```shell
WORKLOAD =====> facet_epirus_3_input_json.json

==================> 1 executor instance
https://regale.ryax.io/app/runs/WorkflowRun-1713537779-k5mbnslw
Time (seconds) to execute WITH SPARK:  764.169778585434


==================> 4 executor instances
https://regale.ryax.io/app/runs/WorkflowRun-1713539049-wkueob75
Time (seconds) to execute WITH SPARK:  459.43426060676575




WORKLOAD =====> flow_dem_450_input_json.json

==================> 1 executor instance
https://regale.ryax.io/app/runs/WorkflowRun-1713547705-coorqsx5
ERROR output was not retrieved for this one
Time (seconds) to execute WITH SPARK:  19977.477568626404


==================> 4 executor instances
https://regale.ryax.io/app/runs/WorkflowRun-1713540703-x2wqohpq
ERROR output was not retrieved for this one
Time (seconds) to execute WITH SPARK:  6475.543595790863
```


## Kubernetes Grid5000 Controlled

### flow_dem_450_input_json\.json

![time (s) over executors](img/execution_time_scio.png "Execution time over spark executors")
![speedup](img/speedup_scio.png "Speedup")

```sh
./cli.py --server=localhost:8081 $FACET_HOME/ryax_metadata.yaml \
--arule=$FACET_HOME/inputs/arule.csv \
--crule=$FACET_HOME/inputs/crule.csv \
--input_json=$HOME/regale/big_input/facet_dem_900_input_json.json \
--flow_zipped_files=$HOME/regale/big_input/flow.zip \
--form_zipped_files=$HOME/regale/big_input/form.zip \
--executor_instances=$nprocs \
--executor_image=ryaxtech/spark-on-k8s:facet-0.0.1 \
--executor_pod_template=$FACET_HOME/executor-pod-template.yaml \
--driver_headless_service=sparkpidriver
```


Cluster Dahu nodes

```shell
Intel Xeon Gold 6130
16 cores/CPU (quad core) = 64 cpus
x86_64
192 GiB
240 GB SSD + 480 GB SSD + 4.0 TB HDD
10 Gbps + 100 Gbps Omni-Path


1 executor = 1 core ( 64 per node )
           = 8 GiB  ( max 22 per node )

```

```shell
==================> 1 executor instances
Time (seconds) to execute WITH SPARK:  16001.662389755249
│Time (seconds) to execute WITH SPARK:  15990.654585123062

==================> 4 executor instances
Time (seconds) to execute WITH SPARK:  4400.568695783615

==================> 10 executor instances
Time (seconds) to execute WITH SPARK:  1960.4634051322937

==================> 20 executor instances
Time (seconds) to execute WITH SPARK:  1173.9402441978455

AFTER 22 executors needs more than 1 node (192 GiB / 8 GiB = 22.5) 

==================> 30 executor instances
Time (seconds) to execute WITH SPARK:  946.5748965740204

==================> 40 executor instances
Time (seconds) to execute WITH SPARK:  788.7498733997345

==================> 50 executor instances
Time (seconds) to execute WITH SPARK:  721.9123306274414

==================> 60 executor instances
Time (seconds) to execute WITH SPARK:  712.2991681098938

==================> 70 executor instances
Time (seconds) to execute WITH SPARK:  638.9961581230164

==================> 80 executor instances
Time (seconds) to execute WITH SPARK:  717.8205907344818 (probably warmup)
Time (seconds) to execute WITH SPARK:  639.7168052196503

==================> 90 executor instances
Time (seconds) to execute WITH SPARK:  652.7418339252472

==================> 100 executor instances
Time (seconds) to execute WITH SPARK:  570.7300152778625

==================> 110 executor instances
Time (seconds) to execute WITH SPARK:  571.6055176258087

==================> 120 executor instances
Time (seconds) to execute WITH SPARK:
```

```logs
(.venv) pevelho@fgrenoble:~/ryax-action-wrappers/wrappers/python3-grpcv1$ ./cli.py --server=localhost:8081 $FACET_HOME/ryax_metadata.yaml --arule=$FACET_HOME/inputs/arule.csv --crule=$FACET_HOME/inputs/crule.csv --input_json=$HOME/regale/big_input/facet_dem_450_input_json.json --flow_zipped_files=$HOME/regale/big_inp
ut/flow.zip --form_zipped_files=$HOME/regale/big_input/form.zip --executor_instances=80 --executor_image=ryaxtech/spark-on-k8s:facet-0.0.1 --executor_pod_template=$FACET_HOME/executor-pod-template.yaml --driver_headless_service=sparkpidriver
Starting client, connecting to localhost:8081...                               
Module Wrapper version: 0.1.0                                                  
Init response received:                                                        
status: DONE                                                                   
executor_id: "facet-mapper-kube-14-0-csw4-857f87c657-jk864"                    
log_delimiter: "38cdd2ab-2ba9-4c43-9f6b-6725b94a1a12"                          

Sending a run request...                                                       
New Response with status: ERROR                                                
Execution started at 1713786287.377909 and ended at 1713787130.915989          


(.venv) pevelho@fgrenoble:~/ryax-action-wrappers/wrappers/python3-grpcv1$ ./cli.py --server=localhost:8081 $FACET_HOME/ryax_metadata.yaml --arule=$FACET_HOME/inputs/arule.csv --crule=$FACET_HOME/inputs/crule.csv --input_json=$HOME/regale/big_input/facet_dem_450_input_json.json --flow_zipped_files=$HOME/regale/big_inp
ut/flow.zip --form_zipped_files=$HOME/regale/big_input/form.zip --executor_instances=40 --executor_image=ryaxtech/spark-on-k8s:facet-0.0.1 --executor_pod_template=$FACET_HOME/executor-pod-template.yaml --driver_headless_service=sparkpidriver                                           
Starting client, connecting to localhost:8081...                               
Module Wrapper version: 0.1.0                                                  
Init response received:                                                        
status: DONE                                                                   
executor_id: "facet-mapper-kube-14-0-csw4-857f87c657-jk864"                    
log_delimiter: "fac80bd6-c4e1-415c-b606-ae3c06ab9a03"                          

Sending a run request...                                                       
New Response with status: ERROR                                                
Execution started at 1713787221.71397 and ended at 1713788113.429501           


(.venv) pevelho@fgrenoble:~/ryax-action-wrappers/wrappers/python3-grpcv1$ .
/cli.py --server=localhost:8081 $FACET_HOME/ryax_metadata.yaml --arule=$FACET_HOME/inputs/arule.csv --crule=$FACET_HOME/inputs/crule.csv --input_json=$HOME/regale/big_input/facet_dem_450_input_json.json --flow_zipped_files=$HOME/regale/big_input/flow.zip --form_zipped_files=$HOME/regale/big_input/form.zip --executor_
instances=4 --executor_image=ryaxtech/spark-on-k8s:facet-0.0.1 --executor_pod_template=$FACET_HOME/executor-pod-template.yaml --driver_headless_service=sparkpidriver                                                    
Starting client, connecting to localhost:8081...                               
Module Wrapper version: 0.1.0                                                  
Init response received:                                                        
status: DONE                                                                   
executor_id: "facet-mapper-kube-14-0-csw4-857f87c657-jk864"                    
log_delimiter: "07e0cec8-bb94-4324-a130-468eee6e55b6"                          

Sending a run request...                                                       
New Response with status: ERROR                                                

========>10                                                                                                                                                                                                                                                                     
Starting client, connecting to localhost:8081...                                                                                                                                                                                                                                
Module Wrapper version: 0.1.0                                                                                                                                                                                                                                                   
Init response received:                                                                                                                                                                                                                                                         
status: DONE                                                                                                                                                                                                                                                                    
executor_id: "facet-mapper-kube-14-0-csw4-857f87c657-jk864"                                                                                                                                                                                                                     
log_delimiter: "2090b5b4-7b79-424d-bfa6-3612419b13bf"                                                                                                                                                                                                                           
                                                                                                                                                                                                                                                                                
Sending a run request...                                                                                                                                                                                                                                                        
New Response with status: ERROR                                                                                                                                                                                                                                                 
Execution started at 1713795720.116807 and ended at 1713797774.738737                                                                                                                                                                                                           
========>30                                                                                                                                                                                                                                                                     
Starting client, connecting to localhost:8081...                                                                                                                                                                                                                                
Module Wrapper version: 0.1.0                                                                                                                                                                                                                                                   
Init response received:                                                                                                                                                                                                                                                         
status: DONE                                                                                                                                                                                                                                                                    
executor_id: "facet-mapper-kube-14-0-csw4-857f87c657-jk864"                                                                                                                                                                                                                     
log_delimiter: "7d6765d3-74c0-41bd-b1b9-97a832561d4d"                                                                                                                                                                                                                           
                                                                                                                                                                                                                                                                                
Sending a run request...                                                                                                                                                                                                                                                        
New Response with status: ERROR                                                                                                                                                                                                                                                 
Execution started at 1713797775.086431 and ended at 1713798819.20374                                                                    
========>50                                                         
Starting client, connecting to localhost:8081...                    
Module Wrapper version: 0.1.0                                       
Init response received:                                             
status: DONE                                                        
executor_id: "facet-mapper-kube-14-0-csw4-857f87c657-jk864"         
log_delimiter: "85ea182d-b5fb-4d67-a5dd-dd6a1558f35b"               

Sending a run request...                                            
New Response with status: ERROR                                     
Execution started at 1713798819.542557 and ended at 1713799645.472114                                                                   
========>60                                                         
Starting client, connecting to localhost:8081...                    
Module Wrapper version: 0.1.0                                       
Init response received:                                             
status: DONE                                                        
executor_id: "facet-mapper-kube-14-0-csw4-857f87c657-jk864"         
log_delimiter: "d32fa850-7746-412b-9823-5c4b996846c3"               

Sending a run request...                                            
New Response with status: ERROR                                     
Execution started at 1713799645.817365 and ended at 1713800462.122562                             
Execution started at 1713799645.817365 and ended at 1713800462.122562
========>70
Starting client, connecting to localhost:8081...
Module Wrapper version: 0.1.0
Init response received:
status: DONE
executor_id: "facet-mapper-kube-14-0-csw4-857f87c657-jk864"
log_delimiter: "b37044d4-c397-49ba-8efb-cba4443969ca"

Sending a run request...
New Response with status: ERROR
Execution started at 1713800462.5557 and ended at 1713801209.72115
========>90
Starting client, connecting to localhost:8081...
Module Wrapper version: 0.1.0
Init response received:
status: DONE
executor_id: "facet-mapper-kube-14-0-csw4-857f87c657-jk864"
log_delimiter: "8e90fa10-bb8b-4a98-a7cb-28dfa73c23ec"

Sending a run request...
qNew Response with status: ERROR
Execution started at 1713801210.059664 and ended at 1713801974.866053
========>100
Starting client, connecting to localhost:8081...
Module Wrapper version: 0.1.0
Init response received:
status: DONE
executor_id: "facet-mapper-kube-14-0-csw4-857f87c657-jk864"
log_delimiter: "13e15e8d-9ee0-4192-8c79-1c9e1e2ece8c"

Sending a run request...
^_^_^_^_New Response with status: ERROR
Execution started at 1713801975.306272 and ended at 1713802660.600673

```

### facet_dem_900_input_json.json

![time (s) over executors](img/execution_time_scio_huge.png "Execution time over spark executors")
![speedup](img/speedup_scio_huge.png "Speedup")

```sh
for nprocs in 110 100 90 80 70 60 50 40 30 20 10 1 ; do 
    ./cli.py --server=localhost:8081 $FACET_HOME/ryax_metadata.yaml \
    --arule=$FACET_HOME/inputs/arule.csv \
    --crule=$FACET_HOME/inputs/crule.csv \
    --input_json=$HOME/regale/huge_input/facet_dem_900_input_json.json \
    --flow_zipped_files=$HOME/regale/huge_input/flow.zip \
    --form_zipped_files=$HOME/regale/huge_input/form.zip \
    --executor_instances=$nprocs \
    --executor_image=ryaxtech/spark-on-k8s:facet-0.0.1 \
    --executor_pod_template=$FACET_HOME/executor-pod-template.yaml \
    --driver_headless_service=sparkpidriver
done
```
