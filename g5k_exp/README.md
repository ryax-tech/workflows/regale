
# nix-compose

## Install nix-compose on grid5000

```sh
# this branch fixes when getting http 404 with install-nix helper
git clone https://github.com/pedrovelho/nixos-compose.git
pip install nixos-compose

# You might want to add this on your .bashrc
cat >> ~/.bashrc <<EOF
export PATH=$PATH:$HOME/.local/bin
EOF

# Make the exeutable available
source ~/.bashrc

nxc helper install-nix

# Add some nix configuration
mkdir -p ~/.config/nix
cat > ~/.config/nix/nix.conf <<EOF
experimental-features = nix-command flakes
EOF

nix --version
```

## Build nix-compose image

Get a working machine


```sh
oarsub -I # wait for the reservation to start
source ~/.bashrc

nxc build -C oar::g5k-nfs-store
```


# Deploy

## Start on grid5000

```sh
export TOTAL_NODES=5
export TOTAL_HOURS=7 #only in hours for now


# To manually do the reservation uncomment the lines below
# oarsub -l nodes=$TOTAL_NODES,walltime=$TOTAL_HOURS:0 "$(nxc helper g5k_script) $TOTAL_HOURSh"
# oarsub -u 

# Alternatively, use the one liner to get OAR_JOB_ID env var
export $(oarsub -l cluster=1/nodes=$TOTAL_NODES,walltime=$TOTAL_HOURS:0:0 "$(nxc helper g5k_script) $TOTAL_HOURSh" | grep OAR_JOB_ID)


# Wait for the job to be on R, running state
# You can also replace OAR_JOB_ID manually
oarstat -j $OAR_JOB_ID -J | \
  jq --raw-output 'to_entries | .[0].value.assigned_network_address | .[]' \
  > machines
  
# Install the kubernetes platform
cd $HOME/regale-nixos-compose.oar-team/bebida/
let "KUBE_NODES = TOTAL_NODES - 1"
nxc start -C oar::g5k-nfs-store -r node=$KUBE_NODES -m machines

```

## Configure kubernetes

* create kubernetes namespaces ryaxns-execs
* add service-account
* check service-account permissions
* add headless-service

```shell
cd $HOME/regale-nixos-compose.oar-team/bebida/
# retrieve the k3s.yaml config
nxc connect server
# change the /home/pevelho for your home on grid 5000
cp /etc/rancher/k3s/k3s.yaml /home/pevelho
exit

export KUBECONFIG=$HOME/k3s.yaml
# create headless services and such
kubectl apply -f ryax-execs-namespace.yaml
kubectl apply -f spark-rbac.yaml
kubectl apply -f spark-headless-service.yaml
```


Test if the service account has the right permissions.

```shell
export SERVICE_ACCOUNT_NAME="spark"
for op in "list" "delete" "create" "update"
do 
    for resource in "pods" "services" "configMaps" "persistentVolumeClaims"
    do 
      echo "Checking for $op on $resource =====>" `kubectl auth can-i $op $resource --as=system:serviceaccount:ryaxns-execs:$SERVICE_ACCOUNT_NAME -n ryaxns-execs`
    done
done
```
