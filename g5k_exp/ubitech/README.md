
# Performance Evaluation

* deploy pod on namespace ryaxns-execs
* create service on ryaxns-execs

```shell
for i in 80 70 60 50 40 30 20 10 8 4 1 ; do 
  echo "==================> $i EXECUTORS"
  time ./cli.py --server=localhost:8081 \
    $ASSETS_HOME/ryax_metadata.yaml \
    --executor_instances=$i \
    --executor_image="ryaxtech/spark-on-k8s:assets-vulnerability-0.0.1" \
    --limit=3 \
    --spark_executor_memory=8g \
    --spark_executor_cpu=2 \
    --application_jar=$ASSETS_HOME/assetvulnerabilities.jar
done > ~/regale/g5k_exp/ubitech/limit-big-index-3.logs
```

time ./cli.py --server=localhost:8081 \
    $ASSETS_HOME/ryax_metadata.yaml \
    --executor_instances=20 \
    --executor_image="ryaxtech/spark-on-k8s:assets-vulnerability-0.0.1" \
    --limit=3 \
    --spark_executor_memory=8g \
    --spark_executor_cpu=2 \
    --application_jar=$ASSETS_HOME/assetvulnerabilities.jar

## Deploy pod

```shell
kubectl apply -f facet-pod.yaml
kubectl apply -f facet-service.yaml
```

## Setup ryax-cli

Install wrapper.

```sh
git clone wrapper
cd wrapper
python -m venv .venv
source .venv/bin/activate
poetry install
poetry shell

# go to cli directory
cd ./wrappers/python3-grpcv1/ 

# check command is running
./cli.py --help
```

## Start Execution
Dispatch execution with client.

```shell
export ASSETS_HOME=/home/pevelho/regale/actions/ubitech/assets_vulnerability
./cli.py --server=localhost:8081 $ASSETS_HOME/ryax_metadata.yaml \
  --executor_instances=100 \
  --application_jar=$ASSETS_HOME/assetvulnerabilities.jar \
  --executor_image=ryaxtech/spark-on-k8s:assets-vulnerability-0.0.1 \
  --asset=$ASSETS_HOME/default_input/asset.parquet \
  --asset_asset_relationship=$ASSETS_HOME/default_input/assetassetrelationship.parquet \
  --asset_relationship=$ASSETS_HOME/default_input/assetrelationship.parquet \
  --vulnerability=$ASSETS_HOME/default_input/vulnerability.parquet \
  --vulnerability_v31=$ASSETS_HOME/default_input/vulnerabilityv31.parquet \
  --indices=$ASSETS_HOME/default_input/indices.csv \
  --limit=1 \
  --spark_executor_memory=8g \
  --spark_executor_cpu=2
```

# Results

## Kubernetes Grid5000 Controlled

### workload A

```yaml
limit: 5
indices: small
```

![](/home/velho/projects/regale/g5k_exp/ubitech/small_indices/small_limit5.png)

![](/home/velho/projects/regale/g5k_exp/ubitech/small_indices/small_limit5_speeup.png)

```logs
pevelho@fgrenoble:~/ryax-action-wrappers/wrappers/python3-grpcv1$ cat exp_ubitech.logs  | grep Execution\ started
Execution started at 1713961606.3238 and ended at 1713961681.448108
1713961681.448108- 1713961606.3238 
75.124308
Execution started at 1713961681.797437 and ended at 1713961764.606939
1713961764.606939 - 1713961681.797437
82.809502
Execution started at 1713961764.908677 and ended at 1713961827.39555
1713961827.39555 - 1713961764.908677
62.486873
Execution started at 1713961827.731492 and ended at 1713961888.664009
1713961888.664009 - 1713961827.731492
60.932517
Execution started at 1713961888.989297 and ended at 1713961957.479219
1713961957.479219 - 1713961888.989297
68.489922
Execution started at 1713961957.789223 and ended at 1713962029.643395
1713962029.643395 - 1713961957.789223
71.854172
Execution started at 1713962029.947759 and ended at 1713962103.580551
1713962103.580551 - 1713962029.947759
73.632792
Execution started at 1713962103.90587 and ended at 1713962181.535349
1713962181.535349 - 1713962103.90587
77.629479
Execution started at 1713962181.892464 and ended at 1713962266.537262
1713962266.537262 - 1713962181.892464
84.644798
Execution started at 1713962266.908353 and ended at 1713962356.959198
1713962356.959198-1713962266.908353
90.050845
Execution started at 1713962357.294285 and ended at 1713962454.601656
1713962454.601656-1713962357.294285
97.307371
Execution started at 1713962454.992801 and ended at 1713962552.659606
1713962552.659606-1713962454.992801
97.666805
1713962650.637248-1713962552.971032
97.666216
```

```shell
executors;time
1;75.124308
4;82.809502
8;62.486873
10;60.932517
20;68.489922
30;71.854172
40;73.632792
50;77.629479
60;84.644798
70;90.050845
80;97.307371
90;97.666805
100;97.666216
```

```yaml
limit: 5
indices: small

```

```csv
executors;time
1;  149.932790
4;  150.304408
8;  179.90131
10; 148.355670
20; 163.583111
30; 220.835354
40; 182.515279
50; 179.394407
60; 233.209938
70; 224.012476
80; 194.362504
```

```logs
for i in 1 4 8 10 20 30 40 50 60 70 80 ; do echo "==================> $i EXECUTORS" ; ./cli.py --server=localhost:8081 $ASSETS_HOME/ryax_metadata.yaml --executor_instances=$i --executor_image="ryaxtech/spark-on-k8s:assets-vulnerability-0.0.1" --limit=5 --spark_executor_memory=8g --spark_executor_cpu=2 --application_jar=$ASSETS_HOME/assetvulnerabilities.jar --indices=$ASSETS_HOME/default_input/indices.csv ; done > limit-5.logs

Execution started at 1713967654.101571 and ended at 1713967804.034361
Execution started at 1713967804.420284 and ended at 1713967954.724692
Execution started at 1713967955.0575 and ended at 1713968134.95881
Execution started at 1713968135.317938 and ended at 1713968283.673608
Execution started at 1713968284.043945 and ended at 1713968447.627056
Execution started at 1713968448.063505 and ended at 1713968668.898859
Execution started at 1713968669.316688 and ended at 1713968851.831967
Execution started at 1713968852.166738 and ended at 1713969031.561145
Execution started at 1713969031.927314 and ended at 1713969265.137252
Execution started at 1713969265.472068 and ended at 1713969489.484544
Execution started at 1713969489.894789 and ended at 1713969684.257293

1713967804.034361 - 1713967654.101571
149.932790
1713967954.724692-1713967804.420284
150.304408
1713968134.95881 - 1713967955.0575
179.90131
1713968283.673608 - 1713968135.317938
148.355670
1713968447.627056 - 1713968284.043945
163.583111
1713968668.898859 - 1713968448.063505
220.835354
1713968851.831967 - 1713968669.316688
182.515279
1713969031.561145 - 1713968852.166738
179.394407
1713969265.137252 - 1713969031.927314
233.209938
1713969489.484544 - 1713969265.472068
224.012476
1713969684.257293 -  1713969489.894789
194.362504

149.932790
150.304408
179.90131
148.355670
163.583111
220.835354
182.515279
179.394407
233.209938
224.012476
194.362504
```

### workload A+

![](/home/velho/projects/regale/g5k_exp/ubitech/big_indices/big_limit3.png)

![](/home/velho/projects/regale/g5k_exp/ubitech/small_indices/big_limit3_speeup.png)


```shell
1714050813.604453 - 1714049760.335647
1053.268806
1714051194.124992 - 1714050813.933486
380.191506
1714051811.308168 - 1714051194.44864
616.859528
1714052273.577261 - 1714051811.665203
461.912058
1714052901.732926 - 1714052273.930511
627.802415
1714053540.702672 - 1714052902.051287
638.651385
1714054004.199883 - 1714053541.061405
463.138478
1714054398.5694 - 1714054004.532824
394.036576
1714055139.714199 - 1714054398.904988
740.809211
1714055474.144839 - 1714055140.042799
334.102040
1714055856.775757 - 1714055474.472209
382.303548
```

```shell
executors;time
1;  1053.268806
4;  380.191506
8;  616.859528
10; 461.912058
20; 627.802415
30; 638.651385
40; 463.138478
50; 394.036576
60; 740.809211
70; 334.102040
80; 382.303548
```
