#!/usr/bin/env bash

for i in 80 70 60 50 40 30 20 10 8 4 1 ; do
  echo "==================> $i EXECUTORS"
  ./cli.py --server=localhost:8081 \
    $ASSETS_HOME/ryax_metadata.yaml \
    --executor_instances=$i \
    --executor_image="ryaxtech/spark-on-k8s:assets-vulnerability-0.0.1" \
    --limit=5 \
    --spark_executor_memory=8g \
    --spark_executor_cpu=2 \
    --application_jar=$ASSETS_HOME/assetvulnerabilities.jar
done
