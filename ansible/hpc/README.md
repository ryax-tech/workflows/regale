
# Install NFS

First install server, check parameters on the yaml and also on the ansible/hosts inventory.

```sh
ansible-playbook -i inventory/hosts nfs-server.yaml
```

Next install clients.

```sh
ansible-playbook -i inventory/hosts nfs-client.yaml
```

# Create new user

```sh
ansible-playbook -i inventory/hosts add-user.yaml --extra-vars "username=ryax userid=1044"
```
